#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "trie.h"

#define CHAR_TO_INDEX(c) ((int)c - (int)'a')
#define ARRAY_SIZE(a) sizeof(a) / sizeof(a[0])
#define ALPHABET_SIZE (26)

struct Trie
{
    int eh_palavra;
    int count;
    struct Trie *filhos[ALPHABET_SIZE]; //vetor de ponteiros para os 26(ALPHABET_SIZE) filhos que um no pode ter
};

struct Trie *addNode()
{

    struct Trie *node = (struct Trie *)malloc(sizeof(struct Trie));

    for (int i = 0; i < ALPHABET_SIZE; i++)
    {
        node->filhos[i] = NULL;
    }
    node->eh_palavra = 0;
    node->count = 0;
    return node;
}

int eh_palavra(struct Trie *node)
{
    if (node->eh_palavra == 1)
    {
        return node->eh_palavra;
    }
}

void inserir(struct Trie *root, const char *key)
{

    int length = strlen(key);
    struct Trie *aux = root;

    for (int level = 0; level < length; level++)
    {

        int index = CHAR_TO_INDEX(key[level]);

        if (!aux->filhos[index])
            aux->filhos[index] = addNode();
        aux = aux->filhos[index];
    }

    // marca com 1 se for folha (palavra)
    aux->count++;
    aux->eh_palavra = 1;
}

int procurar(struct Trie *root, const char *key)
{
    int level;
    int length = strlen(key);
    int index;

    struct Trie *aux = root;

    for (level = 0; level < length; level++)
    {
        index = CHAR_TO_INDEX(key[level]);

        if (!aux->filhos[index])

            return 0;

        aux = aux->filhos[index];
    }

    return (aux->count);
}

void display(struct Trie *root, struct Hash paval, int level, int n)
{
    int i = 0;
    struct Hash vet[n];

    if (eh_palavra(root) == 1)
    {
        paval.chave[level] = '\0';
        paval.valor = root->count;

        if (paval.valor > vet[i].valor)
        {
            strcpy(vet[i].chave, paval.chave);
            vet[i].valor = paval.valor;
            printf("%s -- qtd: %d\n", vet[i].chave, vet[i].valor);
            i++;
        }
    }

    for (i = 0; i < ALPHABET_SIZE; i++)
    {
        if (root->filhos[i])
        {
            paval.chave[level] = i + 'a';
            display(root->filhos[i], paval, level + 1, n);
        }
    }
}

void tf(struct Trie *root, struct Hash paval, int level, int n, int qtd_pala)
{
    int i = 0;
    struct Hash vet[n];

    if (eh_palavra(root) == 1)
    {
        paval.chave[level] = '\0';
        paval.valor = root->count;

        if (paval.valor > vet[i].valor)
        {
            strcpy(vet[i].chave, paval.chave);
            vet[i].valor = paval.valor;
            printf("%s -- tf: %f\n", vet[i].chave, (float)(vet[i].valor / qtd_pala));
            i++;
        }
    }

    for (i = 0; i < ALPHABET_SIZE; i++)
    {
        if (root->filhos[i])
        {
            paval.chave[level] = i + 'a';
            display_1(root->filhos[i], paval, level + 1, n, qtd_pala);
        }
    }
}